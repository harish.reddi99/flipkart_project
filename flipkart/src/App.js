import React, { Component } from 'react';
import './App.css';
import {Link,Switch,Route,withRouter} from 'react-router-dom';
import Home from './home';
import FormDialog from './login';
import Cart from './cart';
import MI from './mi_products';
class App extends Component {
  
  render() {
    return (
      <Switch>
      {/* <Route exact path="/" component={Home}/> */}
      {/* <Route location={{pathname:"Home"}} exact path="/" component={Home}/>
      <Route location={{pathname:"Projects"}} path="/Projects" component={Projects}/>
      <Route location={{pathname:"Skills"}} path="/Skills" component={Skills}/> */}
      <Route location={{pathname:"Home"}} exact path="/" component={Home}/>
      <Route location={{pathname:"FormDialog"}} path="/FormDialog" component={FormDialog}/>
      <Route location={{pathname:"Cart"}} path="/Cart" component={Cart}/>
      <Route location={{pathname:"MI"}} path="/MI" component={MI}/>
    </Switch>
    );
  }
}

export default App;
