import React, { Component } from 'react';
import './App.css';

class MI extends Component {
  render() {
    return (
      <div className="App">
        <div id="main_div">

<div className="my_div">
<h4>Filters</h4><hr/>
<h5>categories</h5>
<p>Mobile Accesories</p>
<p>Mobile </p><hr/>
<h5>Price</h5>
<div class="slidecontainer">
  <input type="range" min="1" max="100" value="50" class="slider" id="myRange"/>
  {/* <p>Value: <span id="demo"></span></p> */}
</div>

</div>
<div className="my_div_1">
 <h5>MI Mobiles</h5><hr/>
 <div className="container">
  <a href="https://google.com"><div className="row">
  <div className="col-md-3">
  <img className="redmi_class" src="https://rukminim1.flixcart.com/image/312/312/jaij3bk0/mobile/f/x/h/mi-redmi-5a-c3b-original-imafy2gfpyybwyyd.jpeg?q=70"/>
  <label class="checkbox-inline">
      <input type="checkbox" value=""/> Add to Compare
    </label>
  </div>
  <div className="col-md-5">
  <h4>Redmi 5A(Gold,16gb)</h4>
  <ul>
      <li>2Gb ram| 16gb rom|Expandable upto 128gb</li>
      <li>5 inch HD display</li>
      <li>13 megapixel rare camera</li>
      <li>5 megapixel front camera</li>
      <li>3000 mah battery</li>
      <li>Qualcomm snapdragon 425 processor</li>
  </ul>
  </div>
  <div className="col-md-4">
  <h4>&#x20b9; 7799</h4>
  <p>EMI starts from 260/month</p>
  </div>
  </div></a>
  <hr/>
  <a href="https://google.com"><div className="row">
  <div className="col-md-3">
  <img className="redmi_class" src="https://rukminim1.flixcart.com/image/312/312/jiklh8w0/mobile/n/8/r/mi-redmi-y1-lite-mzb5748in-original-imaf6cac3gpe5aqz.jpeg?q=70"/>
  <label class="checkbox-inline">
      <input type="checkbox" value=""/> Add to Compare
    </label>
  </div>
  <div className="col-md-5">
  <h4>Redmi Y1 Lite(Grey,16gb)</h4>
  <ul>
      <li>2Gb ram| 16gb rom|Expandable upto 128gb</li>
      <li>5 inch HD display</li>
      <li>13 megapixel rare camera</li>
      <li>5 megapixel front camera</li>
      <li>3000 mah battery</li>
      <li>Qualcomm snapdragon 425 processor</li>
  </ul>
  </div>
  <div className="col-md-4">
  <h4>&#x20b9; 7908</h4>
  <p>EMI starts from 260/month</p>
  </div>
  </div></a>
  <hr/>
  <a href="https://google.com"><div className="row redmi_row_class">
  <div className="col-md-3">
  <img className="redmi_class" src="https://rukminim1.flixcart.com/image/312/312/jiklh8w0/mobile/a/k/t/mi-redmi-y1-lite-mzb5747in-original-imaf6cacdfgsdjyu.jpeg?q=70"/>
  <label class="checkbox-inline">
      <input type="checkbox" value=""/> Add to Compare
    </label>
  </div>
  <div className="col-md-5">
  <h4>Redmi Y1 Lite(Gold,16gb)</h4>
  <ul>
      <li>2Gb ram| 16gb rom|Expandable upto 128gb</li>
      <li>5 inch HD display</li>
      <li>13 megapixel rare camera</li>
      <li>5 megapixel front camera</li>
      <li>3000 mah battery</li>
      <li>Qualcomm snapdragon 425 processor</li>
  </ul>
  </div>
  <div className="col-md-4">
  <h4>&#x20b9; 7908</h4>
  <p>EMI starts from 260/month</p>
  </div>
  </div></a>
 </div>
</div>
</div>

</div>
    );
  }
}

export default MI;
