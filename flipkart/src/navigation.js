import React, { Component } from 'react';
import './App.css';
// import {Link,Switch,Route,withRouter} from 'react-router-dom';
import {BrowserRouter,Switch,Link,Route} from 'react-router-dom'
import Home from './home';
import FormDialog from './login';
import Cart from './cart';
import MI from './mi_products';
class Navigation extends Component {
  render() {
    return (
      <div className="App">
        <div id="main_div">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
  <Link  to="/"> <a class="navbar-brand" href="#" style={{color:'white'}}>Flipkart</a></Link>
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
      <input class="form-control mr-sm-2 search_bar" type="search" placeholder="Search for products,brands and more" aria-label="Search"/>
      </li>
      <li class="nav-item login_navigation">
      <Link to="/FormDialog" ><a class="nav-link login_nav" href="#" style={{color:'white'}}>Login&Signup</a></Link>
      </li>
      {/* <Link  to="/"class="nav-link" href="#">About 
        <span class="sr-only">(current)</span>
        </Link> */}
      <li class="nav-item more_navigation">
        {/* <a class="nav-link" href="#">More</a> */}
        <div class="dropdown">
  <a class="dropbtn more" style={{color:'white'}}>More</a>
  <div class="dropdown-content more_nav">
   <li> <a href="#">Sell on flipkart</a></li><hr/>
  <li>  <a href="#">24/7 customer support</a></li><hr/>
<li><a href="#">Advertise</a></li><hr/>
<li><a href="#">Download App</a></li>
  </div>
</div>
      </li>
      <li class="nav-item cart_navigation">
       <Link to="/Cart"> <a class="nav-link " href="#"><span className="fa fa-shopping-cart" style={{color:'white'}}>Cart</span></a></Link>
      </li>
    </ul>
  </div>
</nav>
	<div class="flex-container">
<div class="dropdown">
  <span>Electronics <span class="fa fa-angle-down"></span></span>
  <div class="dropdown-content drp_content">
    <div className="electronics_dropdown"><p><a href="#">Samsung</a></p>
   <p> <a href="#">Apple</a></p>
    <p><Link to="/MI"><a href="#"> MI</a></Link></p>
    <p><a href="#">Lg</a></p>
</div>
     <div id="div1"><p><a href="#">Nokia</a></p>
    <p><a href="#">Moto</a></p>
    <p><a href="#">Oppo</a></p>
    <p><a href="#">Vivo</a></p>
    </div>
  </div>
   
</div>
<div class="dropdown">
  <span>Mens <span class="fa fa-angle-down"></span></span>
  <div class="dropdown-content drp_content">
    <div className="mens_clothing">
    	<p><a href="#">Jeans</a></p>
    <p><a href="#">Shirts</a></p>
    <p><a href="#">T-Shirts</a></p>
    <p><a href="#">Shorts</a></p>
</div>
     <div id="div1">
     	<p><a href="#">Shoes</a></p>
    <p><a href="#">Sandals</a></p>
    <p><a href="#">Vests</a></p></div>
  </div>
   
</div>
 <div>Women <span class="fa fa-angle-down"></span></div>
  <div>Baby & Kids <span class="fa fa-angle-down"></span></div>
  <div>Home & Furniture <span class="fa fa-angle-down"></span> </div> 
  <div>Sports <span class="fa fa-angle-down"></span></div> 
  <div>Books <span class="fa fa-angle-down"></span></div> 
  <div> Grocery <span class="fa fa-angle-down"></span></div> 
</div>
  </div>

{/* footer starts from here */}
{/* footer ends here */}
    <Switch>
      {/* <Route exact path="/" component={Home}/> */}
      <Route location={{pathname:"Home"}} exact path="/" component={Home}/>
      <Route location={{pathname:"FormDialog"}} path="/FormDialog" component={FormDialog}/>
      <Route location={{pathname:"Cart"}} path="/Cart" component={Cart}/>
      <Route location={{pathname:"MI"}} path="/MI" component={MI}/>
    </Switch>

<div className="container footer" >
 <div className="row">
  <div className="col-md-5 container">
   <div className="row">
<div className="col-md-3">
<ul style={{listStyleType:'none'}}>
  <li className="main_heading">ABOUT</li>  
  <li>Contact</li>
  <li>About </li>
  <li>Careers</li>
  <li>Flipkart</li>
  <li>Press</li>
</ul>
</div>
<div className="col-md-3">
<ul style={{listStyleType:'none'}}>
<li className="main_heading"> HELP</li>
  <li>Payments</li>
  <li>Shipping </li>
  <li>Returns</li>
  <li>Cancellation</li>
  <li>FAQ</li>
</ul>
</div>
<div className="col-md-3">
<ul style={{listStyleType:'none'}}>
<li className="main_heading"> POLICY</li>
  <li>Return Policy</li>
  <li>Terms Of Use </li>
  <li>Security</li>
  <li>Privacy</li>
  <li>Sitemap</li>
</ul>
</div>
<div className="col-md-3">
<ul style={{listStyleType:'none'}}>
<li className="main_heading">SOCIAL</li>
<li>Facebook</li>
  <li>Twitter </li>
  <li>Youtube</li>
  <li>Google plus</li>
  
</ul>
</div>
   </div>
  </div>
  <div className="col-md-7">
   <div className="row">
   <div className="col-md-6">
   <ul style={{listStyleType:'none'}}>
   <li className="main_heading"> Mail Us:</li>
    <li> Flipkart Internet Pvt Ltd Block B (Begonia),</li>
<li>Ground Floor, Embassy Tech Village,</li>
<li>Outer Ring Road, Devarabeesanahalli Village,</li>
<li>Varthur Hobli, Bengaluru East Taluk,</li>
<li>Bengaluru District,</li>
<li>Karnataka, India, 560103.</li>
</ul>
   </div>
   <div className="col-md-6">
   <ul style={{listStyleType:'none'}}>
   <li className="main_heading"> Registered Office Address:</li>
   <li>Flipkart Internet Private Limited,</li>
   <li>Vaishnavi Summit, Ground Floor, 7th Main,</li>
   <li>80 Feet Road, 3rd Block,</li>
   <li>Koramangala,</li>
   <li>Bengaluru - 560034</li>
   <li>India</li>
   <li>CIN : U51109KA2012PTC066107</li>
   <li>Telephone: 1800 208 9898</li>
   </ul>
   </div>
   </div>
  </div>
</div>
<hr/>

<div className="container sub_footer">
<div className="row">
<div className="col-md-6">
<div className="row">
<div className="col-md-4">
<img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNSIgdmlld0JveD0iMCAwIDE2IDE1Ij4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9IjAlIiB4Mj0iODYuODc2JSIgeTE9IjAlIiB5Mj0iODAuMjAyJSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNGRkQ4MDAiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjRkZBRjAwIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTS0yLTJoMjB2MjBILTJ6Ii8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNhKSIgZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNMTUuOTMgNS42MTRoLTIuOTQ4VjQuMTRjMC0uODE4LS42NTUtMS40NzMtMS40NzMtMS40NzNIOC41NmMtLjgxNyAwLTEuNDczLjY1NS0xLjQ3MyAxLjQ3M3YxLjQ3NEg0LjE0Yy0uODE4IDAtMS40NjYuNjU2LTEuNDY2IDEuNDc0bC0uMDA3IDguMTA1YzAgLjgxOC42NTUgMS40NzQgMS40NzMgMS40NzRoMTEuNzljLjgxOCAwIDEuNDc0LS42NTYgMS40NzQtMS40NzRWNy4wODhjMC0uODE4LS42NTYtMS40NzQtMS40NzQtMS40NzR6bS00LjQyMSAwSDguNTZWNC4xNGgyLjk0OHYxLjQ3NHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yIC0yKSIvPgogICAgPC9nPgo8L3N2Zz4K"/>
Sell On Flipkart
</div>
<div className="col-md-3">
<img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNSIgaGVpZ2h0PSIxNSIgdmlld0JveD0iMCAwIDE1IDE1Ij4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9IjAlIiB4Mj0iODYuODc2JSIgeTE9IjAlIiB5Mj0iODAuMjAyJSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNGRkQ4MDAiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjRkZBRjAwIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTS0zLTNoMjB2MjBILTN6Ii8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNhKSIgZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNMTAuNDkyIDNDNi4zNTMgMyAzIDYuMzYgMyAxMC41YzAgNC4xNCAzLjM1MyA3LjUgNy40OTIgNy41QzE0LjY0IDE4IDE4IDE0LjY0IDE4IDEwLjUgMTggNi4zNiAxNC42NCAzIDEwLjQ5MiAzem0zLjE4IDEyTDEwLjUgMTMuMDg4IDcuMzI3IDE1bC44NC0zLjYwN0w1LjM3IDguOTdsMy42OS0uMzE1TDEwLjUgNS4yNWwxLjQ0IDMuMzk4IDMuNjkuMzE1LTIuNzk4IDIuNDIyLjg0IDMuNjE1eiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMgLTMpIi8+CiAgICA8L2c+Cjwvc3ZnPgo="/>
  Advertise
</div>
<div className="col-md-3">
<img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxOCIgaGVpZ2h0PSIxNyIgdmlld0JveD0iMCAwIDE4IDE3Ij4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9IjAlIiB4Mj0iODYuODc2JSIgeTE9IjAlIiB5Mj0iODAuMjAyJSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNGRkQ4MDAiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjRkZBRjAwIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTS0xLTFoMjB2MjBILTF6Ii8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNhKSIgZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNMTYuNjY3IDVIMTQuODVjLjA5Mi0uMjU4LjE1LS41NDIuMTUtLjgzM2EyLjQ5NyAyLjQ5NyAwIDAgMC00LjU4My0xLjM3NUwxMCAzLjM1bC0uNDE3LS41NjdBMi41MSAyLjUxIDAgMCAwIDcuNSAxLjY2N2EyLjQ5NyAyLjQ5NyAwIDAgMC0yLjUgMi41YzAgLjI5MS4wNTguNTc1LjE1LjgzM0gzLjMzM2MtLjkyNSAwLTEuNjU4Ljc0Mi0xLjY1OCAxLjY2N2wtLjAwOCA5LjE2NkExLjY2IDEuNjYgMCAwIDAgMy4zMzMgMTcuNWgxMy4zMzRhMS42NiAxLjY2IDAgMCAwIDEuNjY2LTEuNjY3VjYuNjY3QTEuNjYgMS42NiAwIDAgMCAxNi42NjcgNXptMCA2LjY2N0gzLjMzM3YtNWg0LjIzNEw1LjgzMyA5LjAyNWwxLjM1Ljk3NSAxLjk4NC0yLjdMMTAgNi4xNjdsLjgzMyAxLjEzMyAxLjk4NCAyLjcgMS4zNS0uOTc1LTEuNzM0LTIuMzU4aDQuMjM0djV6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMSAtMSkiLz4KICAgIDwvZz4KPC9zdmc+Cg=="/>
Gift Cards
</div>
<div className="col-md-2">
<img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNSIgaGVpZ2h0PSIxNSIgdmlld0JveD0iMCAwIDE1IDE1Ij4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9IjAlIiB4Mj0iODYuODc2JSIgeTE9IjAlIiB5Mj0iODAuMjAyJSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNGRkQ4MDAiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjRkZBRjAwIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTS0yLTNoMjB2MjBILTJ6Ii8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNhKSIgZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNOS41IDNDNS4zNiAzIDIgNi4zNiAyIDEwLjUgMiAxNC42NCA1LjM2IDE4IDkuNSAxOGM0LjE0IDAgNy41LTMuMzYgNy41LTcuNUMxNyA2LjM2IDEzLjY0IDMgOS41IDN6bS43NSAxMi43NWgtMS41di0xLjVoMS41djEuNXptMS41NTMtNS44MTNsLS42NzYuNjljLS41NC41NDgtLjg3Ny45OTgtLjg3NyAyLjEyM2gtMS41di0uMzc1YzAtLjgyNS4zMzgtMS41NzUuODc3LTIuMTIzbC45My0uOTQ1Yy4yNzgtLjI3LjQ0My0uNjQ1LjQ0My0xLjA1NyAwLS44MjUtLjY3NS0xLjUtMS41LTEuNVM4IDcuNDI1IDggOC4yNUg2LjVhMyAzIDAgMSAxIDYgMGMwIC42Ni0uMjcgMS4yNi0uNjk3IDEuNjg4eiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIgLTMpIi8+CiAgICA8L2c+Cjwvc3ZnPgo="/>
Help
</div>
</div>
</div>


<div className="col-md-6">
<div className="row">
<div className="col-md-5">© 2007-2018 Flipkart.com</div>
<div className="col-md-7">
<img src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/payment-method_2dd397.svg"/>
</div>
</div>
</div>
</div>
</div>
</div>
      </div>
    );
  }
}

export default Navigation;
