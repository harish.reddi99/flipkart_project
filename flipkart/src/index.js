import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {BrowserRouter} from 'react-router-dom'
import App from './App';
import MI from './mi_products';
import ViewImgProduct from './view_image_products';
import FormDialog from './login';
import Signup from './signup';
import Cart from './cart';
import Navigation from './navigation';
import Home from './home';
import Loginorsignup from './login_signup';
import * as serviceWorker from './serviceWorker';

 ReactDOM.render(
     <BrowserRouter>
        <Navigation />
     </BrowserRouter>,
 document.getElementById('root'));
// ReactDOM.render(<Loginorsignup />, document.getElementById('root'));
// ReactDOM.render(<ViewImgProduct />, document.getElementById('root'));
// ReactDOM.render(<FormDialog />, document.getElementById('root'));
// ReactDOM.render(<Cart/>, document.getElementById('root'));
// ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<Home />, document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
