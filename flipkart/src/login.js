// import React, { Component } from 'react';
import './App.css';

import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default class FormDialog extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div className="dialog_div">
      
        <Button onClick={this.handleClickOpen}>Open form dialog</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
        
        
          {/* <DialogTitle id="form-dialog-title">Login</DialogTitle> */}
          <DialogContent>
          <div className="login_div">
          <DialogTitle id="form-dialog-title">Login</DialogTitle>
          <DialogContent>
            Get access to your Orders,Wishlist and Recommendations 
          </DialogContent>
          </div>
            {/* <DialogContentText>
              To subscribe to this website, please enter your email address here. We will send
              updates occasionally.
            </DialogContentText> */}
            <div className="login_div_1">
            <TextField autoFocus margin="dense" id="name" label="Email Address" type="email" fullWidth />
            <TextField autoFocus margin="dense" id="pwd" label="Password" type="password" fullWidth />
            <button className="btn-primary login_btn">Login</button>
            <button className="btn-primary signup_btn" style={{backgroundColor:'white',color:'blue'}}>Sign Up</button>
            </div>
          </DialogContent>
          {/* <DialogActions> */}
            {/* <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleClose} color="primary">
              Subscribe
            </Button> */}
          {/* </DialogActions> */}
          
        </Dialog>
      </div>
    );
  }
}
