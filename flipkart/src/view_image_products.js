import React, { Component } from 'react';
import './App.css';

class ViewImgProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images:[
        'https://rukminim1.flixcart.com/image/832/832/jdhp47k0/mobile/x/8/3/redmi-note-5-pro-na-original-imaf2asgzugtjaft.jpeg?q=70',
        'https://rukminim1.flixcart.com/image/832/832/jddesnk0/mobile/e/h/e/redmi-e7s-note-5-pro-na-original-imaf2ashgckkfqxx.jpeg?q=70',
        'https://rukminim1.flixcart.com/image/832/832/jdhp47k0/mobile/e/h/e/redmi-note-5-pro-na-original-imaf2ashuwmatzkp.jpeg?q=70',
        // 'https://rukminim1.flixcart.com/image/832/832/jdhp47k0/mobile/e/h/e/redmi-note-5-pro-na-original-imaf2ashhyprwwrf.jpeg?q=70'
      ],
      currentImage: 0,
    };
  }
  //  fadeImage(e) {
    //  e.preventDefault();
  //  this.setState({currentImage: (this.state.currentImage ) % this.state.images.length})
  // }
  
  render() {
   
    return (
      <div className="App">
        <div id="main_div">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <a class="navbar-brand" href="#" style={{color:'white'}}>Flipkart</a>
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
      <input class="form-control mr-sm-2 search_bar" type="search" placeholder="Search for products,brands and more" aria-label="Search"/>
      </li>
      <li class="nav-item login_navigation">
        <a class="nav-link login_nav" href="#" style={{color:'white'}}>Login&Signup</a>
      </li>
      <li class="nav-item more_navigation">
        {/* <a class="nav-link" href="#">More</a> */}
        <div class="dropdown">
  <a class="dropbtn more" style={{color:'white'}}>More</a>
  <div class="dropdown-content more_nav">
   <li> <a href="#">Sell on flipkart</a></li><hr/>
  <li>  <a href="#">24/7 customer support</a></li><hr/>
<li><a href="#">Advertise</a></li><hr/>
<li><a href="#">Download App</a></li>
  </div>
</div>
      </li>
      <li class="nav-item cart_navigation">
        <a class="nav-link " href="#"><span className="fa fa-shopping-cart" style={{color:'white'}}>Cart</span></a>
      </li>
    </ul>
  </div>
</nav>
	<div class="flex-container">
<div class="dropdown">
  <span>Electronics <span class="fa fa-angle-down"></span></span>
  <div class="dropdown-content drp_content">
    <div className="samsung"><a href="#">Samsung</a>
    <p>Mi</p>
    <p>Lg</p>
    <p>Lg</p>
</div>
     <div id="div1"><p>Hello World!</p>
    <p>Hello World!</p>
    <p>Hello World!</p></div>
  </div>
   
</div>
<div class="dropdown">
  <span>Mens <span class="fa fa-angle-down"></span></span>
  <div class="dropdown-content drp_content">
    <div className="mens_clothing">
    	<p><a href="#">Jeans</a></p>
    <p><a href="#">Shirts</a></p>
    <p><a href="#">T-Shirts</a></p>
    <p><a href="#">Shorts</a></p>
</div>
     <div id="div1">
     	<p><a href="#">Shoes</a></p>
    <p><a href="#">Sandals</a></p>
    <p><a href="#">Vests</a></p></div>
  </div>
   
</div>
 <div>Women <span class="fa fa-angle-down"></span></div>
  <div>Baby & Kids <span class="fa fa-angle-down"></span></div>
  <div>Home & Furniture <span class="fa fa-angle-down"></span> </div> 
  <div>Sports <span class="fa fa-angle-down"></span></div> 
  <div>Books <span class="fa fa-angle-down"></span></div> 
  <div> Grocery <span class="fa fa-angle-down"></span></div> 
</div>
  </div>
<div className="image_full_view">
{/* <div className="small_img_div"> */}
{/* </div> */}
    {/* <section>
            <div className="small_img_div" onClick={this.fadeImage.bind(this)}></div>
            <img src={this.state.images[this.state.currentImage]}/>
      </section> */}
 {/* <div className="small_img_div_1 ">
 {/* {this.state.images} */}
{/* </div>  */}
{/* <div className="small_img_div_2"> */}
{/* </div> */}
{/* <div className="small_img_div_3"> */}
{/* </div> */}
 {
 this.state.images.map((image,index) => {
      return <div className="small_img_div" onClick={()=>{this.setState({currentImage:index})}}>  
                <img key={index} src={(image)} alt="mobile image" className="small_imageChildDiv"/>
              </div>
   })
}
{/* src={th} */}
<div className="image_zoom"> 
    <img src={this.state.images[this.state.currentImage]} className="image_preview"/> 
</div> 
 
</div>
{/* <button type="button" class="btn btn-warning addtocart">Add To Cart</button> */}
    {/* <button type="button" class="btn btn-warning buy_now">Buy Now</button> */}
<div className="image_content">
<h5 className="redmi_description">Redmi Y1 Lite(16Gb,Gold) (2 GB Ram)</h5>
<h4>&#x20b9; 7,799</h4>
<p>Brand warranty 1 year for mobile and 6 months for accessories</p>
<div className="container">
<div className="row">
<div className="col-md-2">Highlights</div>
<div className="col-md-5">
<ul>
    <li>2 GB ram | 16 GB rom | Expandable upto 120 Gb</li>
    <li>12.7cm(5 inch) HD Display</li>
    <li>13 MP Rear Camera | 5 MP Front Camera</li>
    <li>3000 MAH Li-polymeter battery</li>
    <li>Qualcomm Snapdragon 425 Processor</li>
</ul>
</div>
</div>
<div className="row">
<div className="col-md-2">Description</div>
<div className="col-md-9">
Redmi 5A boasts of a beautiful fully-laminated 12.7 cm (5) HD display. Upto 8 days standby time with 3000 mAh Battery.Take gorgeous group photos and scenic shots on the all-new Redmi 5A. It is equipped with a fast focusing 13MP camera that helps you capture sharp and crisp photos.Qualcomm's Snapdragon 425 64-bit quad-core processor is great for daily use and performs well even when you're playing visually intensive games.
</div>
</div>
</div>
<div className="container">
<div className="row">
<table style={{border:'2px solid black'}}>
    <tr style={{border:'2px solid black'}}>
      <th style={{border:'2px solid black'}}>Specifications</th>
    </tr>
    <tr style={{border:'2px solid black'}}>
        <td>In the Box</td>
        <td>Handset, Power Adapter, USB Data Cable, Ultra Thin Case, Warranty Card, Getting Started Guide, Pin</td>
    </tr>
    <tr>
        <td>Model Number</td>
        <td>MZB6079IN / MZB6087IN</td>
    </tr>
    <tr>
        <td>Model Name</td>
        <td>Redmi Note 5 Pro</td>
    </tr>
</table>
</div>
</div>
</div>
</div>
    );
  }
}

export default ViewImgProduct;
