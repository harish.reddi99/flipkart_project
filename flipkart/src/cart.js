import React, { Component } from 'react';
import './App.css';

class Cart extends Component {
  
  render() {
    return (
      <div className="App">
        <div id="main_div">

<div className="container cart_mainDiv">
<div className="row">
<div className="col-md-8 cart_itemsDiv">

<div className="row">
<div className="col-md-2 cart_header"><h6>MY CART</h6></div>
<div className="col-md-6 cart_header"></div>
<div className="col-md-2 cart_header"><b>Pin 524201</b></div>
<div className="col-md-2 change cart_header"><b>Change</b></div>
</div>
<hr/>
<div className="row">
<div className="col-md-3">
<img src="https://rukminim1.flixcart.com/image/100/100/jph83gw0/watch/u/5/h/abx2064-br-brown-abrexo-original-imafbpmyj8978cm7.jpeg?q=90"/>
</div>
<div className="col-md-5"><h6>Abrexo Abx2064-BR BROWN Day & Date Watch - For Men
Seller: ABREXO</h6>
<p className="seller">Seller: ABREXO</p>
<p><b>₹630</b>   <del>₹3,998</del>  <h6 className="bold_offers">84% Off 2 Offers Available</h6></p>
</div>
<div className="col-md-4">Delivery in 2 Days, Sat | Free</div>
</div>
<div className="row cart_continueShopping">
<div className="col-md-4"></div>
<div className="col-md-4"><button type="button" class="btn btn-default cart_btn">Continue Shopping</button></div>
<div className="col-md-4"><button type="button" class="btn btn-warning cart_btn">Place Order</button></div>
</div>
</div>
<div className="col-md-4 cart_priceDiv">
<div>
  PRICE DETAILS
</div><hr/>
<div>
  Price (1 Item) 299
</div>
<div>Delivery Charges</div><hr style={{border: '1px dashed grey'}}/>
<div>Amount Payable</div><hr/>
<div>Your Total Savings on this order ₹1,660</div>
</div>
</div>
</div>
<div className="container coinsImage_container ">
<div className="row">
<div className="col-md-8"></div>
<div className="col-md-4 cart_backgroungImg">
<img className="cart_coins_image" src="https://rukminim1.flixcart.com/lockin/736/219/images/Coins-Banner-without_know-more__1533290139.png?q=50"/> 
</div>
</div>
</div>
<div className="container">
<div className="row">
<div className="col-md-8"></div>
<div className="col-md-4 "><img className="icon_image" src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/shield_a7ea6b.png"/><span>Safe and Secure Payments. Easy returns. 100% Authentic products.</span></div>
</div>
</div>
<hr/>
<div className="container">
<div className="row">
<div className="col-md-6">Policies:Returns Policy | Terms of use | Security | Privacy | Infringement</div>
<div className="col-md-3">© 2007-2018 Flipkart.com.</div>
<div className="col-md-3">Need help? Visit the Help Center</div>
</div>
</div>
  </div>
      </div>
    );
  }
}

export default Cart;
